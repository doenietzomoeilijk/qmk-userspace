/* vim: set path+=~/qmk_firmware/quantum,~/qmk_firmware/users/dnzm */

#include "quantum.h"
#include "dnzm.h"
#include "features/achordion.h"
/* #include "features/achordion.c" */
#include "features/luna.c"
#include "layout.h"
#include <transactions.h>

bool process_record_user(uint16_t keycode, keyrecord_t* record) {
#ifdef CONSOLE_ENABLE
    if (debug_enable) {
        uprintf(
                "KL kc=0x%04X col=%02u row=%02u press=%s time=%5u int=%u count=%u\n",
                keycode,
                record->event.key.col,
                record->event.key.row,
                record->event.pressed ? "pressed " : "released",
                record->event.time,
                record->tap.interrupted,
                record->tap.count);
    }
#endif

    if (!process_achordion(keycode, record)) {
        return false;
    }

    switch (keycode) {
        case KC_SPC:
        case KC_ESC:
            if (record->event.pressed) {
                isJumping  = true;
                showedJump = false;
            } else {
                isJumping = false;
            }
            break;

        case KC_F23:
        case OBR_DN:

            if (record->event.pressed && oled_get_brightness() > 10) {
                oled_set_brightness(oled_get_brightness() - 10);
            }
#ifdef CONSOLE_ENABLE
            uprintf("OLED brightness set to %d", oled_get_brightness());
#endif
            return false;
            break;
        case KC_F24:
        case OBR_UP:
            if (record->event.pressed && oled_get_brightness() <= 200) {
                oled_set_brightness(oled_get_brightness() + 10);
            }
#ifdef CONSOLE_ENABLE
            uprintf("OLED brightness set to %d", oled_get_brightness());
#endif
            return false;
        case KC_F22:
        case T_DBG:
            if (record->event.pressed) {
                debug_enable = !debug_enable;
                //debug_matrix = !debug_matrix;
                //debug_keyboard = !debug_keyboard;
            }
            return false;
            break;
    }

    return true;
}

bool caps_word_press_user(uint16_t keycode) {
    switch (keycode) {
        // Keycodes that continue Caps Word, with shift applied.
        case KC_A ... KC_Z:
        // case KC_MINS:
            add_weak_mods(MOD_BIT(KC_LSFT));  // Apply shift to next key.
            return true;

        // Keycodes that continue Caps Word, without shifting.
        case KC_1 ... KC_0:
        case KC_BSPC:
        case KC_DEL:
        case KC_UNDS:
            return true;

        default:
            return false;  // Deactivate Caps Word.
    }
}

bool achordion_chord(uint16_t tap_hold_keycode, keyrecord_t* tap_hold_record,
                     uint16_t other_keycode, keyrecord_t* other_record) {
    // Allow same-hand alt-spacing and ctrl-tabbing and so on.
    if (other_keycode == NAV_SPC || other_keycode == FUN_TAB) {
        return true;
    }

    // Otherwise, follow the opposite hands rule.
    return achordion_opposite_hands(tap_hold_record, other_record);
}


void matrix_scan_user(void) {
    achordion_task();
}

typedef struct _slave_to_master_t {
    int s2m_data;
} slave_to_master_t;

void sync_caps_word_state_handler(uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data) {
    const master_to_slave_t *m2s = (const master_to_slave_t*)in_data;
    if (m2s->m2s_data == 1) {
        caps_word_on();
    } else {
        caps_word_off();
    }
}

void caps_word_set_user(bool active) {
    if (is_keyboard_master()) {
        master_to_slave_t m2s = {active ? 1 : 0};
        slave_to_master_t s2m = {0};
        if (transaction_rpc_exec(USER_SYNC_A, sizeof(m2s), &m2s, sizeof(s2m), &s2m)) {
            dprintf("synced caps word state");
        }
    }
}

void keyboard_post_init_user(void) {
    // oled_set_brightness(10);
    // Customise these values to desired behaviour
    debug_enable = false;
    debug_matrix = false;
    debug_keyboard = false;
    //debug_mouse=true;

    transaction_register_rpc(USER_SYNC_A, sync_caps_word_state_handler);
}
