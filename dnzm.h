#pragma once

#include QMK_KEYBOARD_H

enum layer_names {
    _QWE,
    _COL,
    _NAV,
    _NUM,
    _SYM,
    _FUN,
    _MOU
};

void spam_caps_state(void);
void spam_mods_state(uint8_t mods, bool lock);

typedef struct _master_to_slave_t {
    int m2s_data;
} master_to_slave_t;

void sync_caps_word_state_handler(uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data);
