/* vim: set cc=9,19,29,39,49,59,69,79,89,99,109,119,129,139,149 path+=~/qmk_firmware */

#pragma once

#include "quantum/keycodes.h"

#define LAYOUT_wrapper(...) LAYOUT(__VA_ARGS__)

#define ___     KC_NO
#define _v_     KC_TRNS

// Home row mods
#define GUI_A   LGUI_T(KC_A)
#define ALT_S   LALT_T(KC_S)
#define CTL_D   LCTL_T(KC_D)
#define SFT_F   LSFT_T(KC_F)
#define SFT_J   RSFT_T(KC_J)
#define CTL_K   RCTL_T(KC_K)
#define ALT_L   LALT_T(KC_L)
#define GUI_SC  LGUI_T(KC_SCLN)

// Additions for Colemak
#define CALT_R   LALT_T(KC_R)
#define CCTL_S   LCTL_T(KC_S)
#define CSFT_T   LSFT_T(KC_T)
#define CSFT_N   RSFT_T(KC_N)
#define CCTL_E   RCTL_T(KC_E)
#define CALT_I   LALT_T(KC_I)
#define CGUI_O   LGUI_T(KC_O)

// Thumbs
#define NAV_ENT LT(2,KC_ENT)
#define NAV_SPC LT(2,KC_SPC)
#define FUN_TAB LT(5,KC_TAB)
#define SYM_ENT LT(4,KC_ENT)
#define NUM_SPC LT(3,KC_SPC)

// Special thingies
#ifndef __ASSEMBLER__
enum custom_keycode {
    OBR_UP = QK_USER,
    OBR_DN,
    T_DBG
};
#endif

// Layer 0
#define QUERTY \
        KC_ESC,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,                         KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS,  \
        KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,                         KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_BSPC,  \
        KC_EQL,   GUI_A,    ALT_S,    CTL_D,    SFT_F,    KC_G,                         KC_H,     SFT_J,    CTL_K,    ALT_L,    GUI_SC,   KC_QUOT,  \
        KC_GRV,   KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     CW_TOGG,  KC_PSCR,  KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_BSLS,  \
                                      KC_LALT,  KC_LGUI,  NAV_SPC,  FUN_TAB,  SYM_ENT,  NUM_SPC,  KC_RALT,  TG(6)

// Layer 1
#define COLEMAK \
        KC_ESC,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,                         KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS,  \
        KC_TAB,   KC_Q,     KC_W,     KC_F,     KC_P,     KC_B,                         KC_J,     KC_L,     KC_U,     KC_Y,     KC_SCLN,  KC_BSPC,  \
        KC_EQL,   GUI_A,    CALT_R,   CCTL_S,   CSFT_T,   KC_G,                         KC_M,     CSFT_N,   CCTL_E,   CALT_I,   CGUI_O,   KC_QUOT,  \
        KC_GRV,   KC_Z,     KC_X,     KC_C,     KC_D,     KC_V,     CW_TOGG,  KC_PSCR,  KC_K,     KC_H,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_BSLS,  \
                                      KC_LALT,  KC_LGUI,  NAV_SPC,  FUN_TAB,  SYM_ENT,  NUM_SPC,  KC_RALT,  TG(6)

// Layer 2
#define NAVIGATION \
        TG(2),    ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        ___,      ___,      ___,      ___,      ___,      ___,                          KC_WBAK,  KC_WH_D,  KC_WH_U,  KC_WFWD,  KC_MUTE,  KC_DEL,   \
        ___,      KC_LGUI,  KC_LALT,  KC_LCTL,  KC_LSFT,  ___,                          KC_LEFT,  KC_DOWN,  KC_UP,    KC_RGHT,  KC_CAPS,  ___,      \
        ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      KC_HOME,  KC_PGDN,  KC_PGUP,  KC_END,   ___,      ___,      \
                                      ___,      ___,      ___,      ___,      ___,      KC_BSPC,  KC_APP,   KC_NO

// Layer 3
#define NUMBERS \
        _v_,      ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        _v_,      KC_LBRC,  KC_7,     KC_8,     KC_9,     KC_RBRC,                      ___,      ___,      ___,      ___,      ___,      _v_,      \
        ___,      KC_MINS,  KC_4,     KC_5,     KC_6,     KC_EQL,                       ___,      KC_RSFT,  KC_RCTL,  KC_LALT,  KC_RGUI,  ___,      \
        ___,      KC_LT,    KC_1,     KC_2,     KC_3,     KC_GT,    ___,      ___,      ___,      ___,      KC_COMM,  KC_DOT,   ___,      ___,      \
                                      ___,      KC_9,     KC_0,     KC_MINS,  ___,      ___,      ___,      KC_NO

// Layer 4
#define SYMBOLS \
        _v_,      ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        _v_,      KC_LCBR,  KC_AMPR,  KC_ASTR,  KC_LPRN,  KC_RCBR,                      ___,      ___,      ___,      ___,      ___,      _v_,      \
        ___,      KC_MINS,  KC_DLR,   KC_PERC,  KC_CIRC,  KC_EQL,                       ___,      KC_RSFT,  KC_RCTL,  KC_LALT,  KC_RGUI,  ___,      \
        ___,      KC_LT,    KC_EXLM,  KC_AT,    KC_HASH,  KC_GT,    ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      \
                                      ___,      KC_LPRN,  KC_RPRN,  KC_UNDS,  ___,      ___,      ___,      KC_NO

// Layer 5
#define FUNCTION \
        KC_SLEP,  KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,                        ___,      KC_F10,   KC_F11,   KC_F12,   ___,      T_DBG,    \
        ___,      ___,      ___,      ___,      ___,      ___,                          ___,      KC_F7,    KC_F8,    KC_F9,    KC_F12,   OBR_UP,   \
        ___,      KC_LGUI,  KC_LALT,  KC_LCTL,  KC_LSFT,  ___,                          ___,      KC_F4,    KC_F5,    KC_F6,    KC_F11,   OBR_DN,   \
        ___,      ___,      ___,      KC_CALC,  ___,      ___,      ___,      ___,      ___,      KC_F1,    KC_F2,    KC_F3,    KC_F10,   ___,      \
                                      ___,      ___,      ___,      ___,      ___,      ___,      ___,      KC_NO

// Layer 6
#define MOUSE \

        TG(6),    ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        OBR_UP,   KC_BRIU,  ___,      KC_WH_U,  ___,      KC_VOLU,                      ___,      ___,      ___,      ___,      KC_ACL2,  OBR_UP,   \
        OBR_DN,   KC_BRID,  KC_WH_L,  KC_WH_D,  KC_WH_R,  KC_VOLD,                      KC_MS_L,  KC_MS_D,  KC_MS_U,  KC_MS_R,  KC_ACL1,  OBR_DN,   \
        ___,      ___,      ___,      ___,      ___,      KC_MUTE,  ___,      ___,      ___,      ___,      ___,      ___,      KC_ACL0,  ___,      \
                                      DF(0),    DF(1),    ___,      ___,      KC_BTN2,  KC_BTN1,  KC_BTN3,  TG(6)

#define TEMPLATE \
        ___,      ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        ___,      ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        ___,      ___,      ___,      ___,      ___,      ___,                          ___,      ___,      ___,      ___,      ___,      ___,      \
        ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___,      \
                                      ___,      ___,      ___,      ___,      ___,      ___,      ___,      ___
