#pragma once

#ifndef __ASSEMBLER__
#include "layout.h"
#endif

// Memory saving
#undef  LOCKING_SUPPORT_ENABLE
#undef  LOCKING_RESYNC_ENABLE
#define LAYER_STATE_8BIT
#define DYNAMIC_KEYMAP_LAYER_COUNT 7

// Split setup
#define EE_HANDS
#define SPLIT_USB_DETECT
#define SPLIT_LAYER_STATE_ENABLE
#define SPLIT_LED_STATE_ENABLE
#define SPLIT_MODS_ENABLE
#define SPLIT_WPM_ENABLE
#define SPLIT_TRANSACTION_IDS_USER USER_SYNC_A, USER_SYNC_B

// Taps and shots
#define TAPPING_TERM 150
#define PERMISSIVE_HOLD
#define TAPPING_FORCE_HOLD_PER_KEY
#define ONESHOT_TAP_TOGGLE 2

#define CAPS_WORD_IDLE_TIMEOUT 1000

#ifdef OLED_DRIVER_ENABLE
#define SPLIT_OLED_ENABLE

//#define OLED_FONT_H "lib/glcdfont_lily.c"
//#define OLED_FONT_H "lib/font-open-sans.c"
#define OLED_FONT_WIDTH 7
#define OLED_FONT_HEIGHT 11
#define OLED_BRIGHTNESS 20
#define OLED_TIMEOUT 3000

//These are not used due to animation, but let's keep them for future
//reference.
#define OLED_FADE_OUT
#define OLED_FADE_OUT_INTERVAL 10
#endif
