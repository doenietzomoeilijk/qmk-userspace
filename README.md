DoeNietZoMoeilijk QMK userspace
===============================

Plunk this into `<qmk_firmware>/users/dnzm`, compile with 
`qmk compile -c -j 12 users/dnzm/dnzm.json`.

Meant for the Lili58, and the Lily58 _only_. For now.
